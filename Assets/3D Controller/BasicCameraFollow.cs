﻿using UnityEngine;

namespace PruebaTecnica
{
    public class BasicCameraFollow : MonoBehaviour
    {
        [SerializeField]
        private Transform target = null;
        [SerializeField]
        private Vector3 positionOffset = Vector3.zero;
        [SerializeField]
        private Vector3 rotationOffset = Vector3.zero;
        [SerializeField]
        private float smooth = 0.5f;

        private Vector3 targetPos = Vector3.zero;

        private void Awake()
        {
            if (target == null)
                target = GameObject.FindGameObjectWithTag("Player")?.transform;
        }

        private void LateUpdate()
        {
            if (target)
            {
                targetPos = target.position + target.forward * positionOffset.z + target.up * positionOffset.y + target.right * positionOffset.x;
                transform.position = Vector3.Lerp(transform.position, targetPos, smooth);

                transform.LookAt(target.position);
                transform.rotation *= Quaternion.Euler(rotationOffset);
            }
        }
    }
}