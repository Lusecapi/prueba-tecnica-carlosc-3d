﻿using UnityEngine;

namespace PruebaTecnica
{
    public class PlayerController : MonoBehaviour
    {

        [SerializeField]
        private float speed = 10;
        [SerializeField]
        private float rotationSpeed = 10;
        [SerializeField]
        private float jumpForce = 10;
        [SerializeField]
        private bool airControl = false;

        private bool jump = false;
        private bool isGrounded = false;

        private float distToGround = 0;
        Rigidbody rb = null;
        BoxCollider boxCollider = null;


        private Vector2 inputs = Vector2.zero;

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
            boxCollider = GetComponentInChildren<BoxCollider>();
        }

        private void Start()
        {
            distToGround = boxCollider.bounds.extents.y;
        }

        private void Update()
        {
            CheckGrounded();
            print(isGrounded);

            inputs.Set(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            inputs.Normalize();
            jump = Input.GetKeyDown(KeyCode.Space) && isGrounded;
        }


        private void FixedUpdate()
        {
            if (jump)
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            }

            if(inputs != Vector2.zero)
            {
                rb.rotation = rb.rotation * Quaternion.Euler(Vector3.up * inputs.x * rotationSpeed * Time.deltaTime);
            }

            if (isGrounded || airControl)
                rb.velocity = transform.forward * speed * inputs.y * Time.fixedDeltaTime;
        }

        private void CheckGrounded()
        {
            Debug.DrawRay(transform.position, Vector3.down * (distToGround + 0.1f), Color.red);
            isGrounded = Physics.Raycast(transform.position, Vector3.down, distToGround + 0.1f);
        }


    }
}